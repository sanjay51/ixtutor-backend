package test;

import ixtutor.security.TokenUtils;
import org.testng.annotations.Test;

/**
 * Created by sanjav on 1/8/17.
 */
public class TokenUtilsTest {

    @Test
    public void createAndVerifyToken() throws Exception {
        String email = "sanjay.verma.nitk@gmail.com";

        String token = TokenUtils.generateToken(email);
        System.out.println(token);
        TokenUtils.verifyToken(token, email);
    }
}
