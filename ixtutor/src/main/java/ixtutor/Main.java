package ixtutor;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import ixtutor.activities.ActivityFactory;
import ixtutor.model.request.Request;

public class Main implements RequestHandler<Request, Response> {
    
    public Response handleRequest(Request request, Context context) {
        return ActivityFactory.getInstance(request).getResponse();
    }
}