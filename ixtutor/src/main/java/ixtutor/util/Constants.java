package ixtutor.util;

/**
 * Created by sanjav on 12/18/16.
 */
public class Constants {
    public static final String PARAM_API = "api";

    public static final String API_getCourseByID = "getCourseByID";
    public static final String API_getAllCoursesMetadata = "getAllCoursesMetadata";
    public static final String API_createCourse = "createCourse";
    public static final String API_updateCourseMetadata = "updateCourseMetadata";
    public static final String API_updateCoursePayload = "updateCoursePayload";
    public static final String table_course = "course";
    public static final String col_courseId = "courseId";
    public static final String col_metadata = "metadata";
    public static final String col_payload = "payload";
    public static final String col_category = "category";
    public static final String col_title = "title";
    public static final String col_oneLineDescription = "oneLineDescription";
    public static final String col_description = "description";
    public static final String col_author = "author";

    public static final String param_token = "token";

    public static final String API_getUserByEmail = "getUserByEmail";
    public static final String API_login = "login";
    public static final String API_signup = "signup";
    public static final String table_user = "user";
    public static final String col_email = "email";
    public static final String col_data = "data";
    public static final String col_id = "id";
    public static final String col_name = "name";
    public static final String col_password = "password";

    public static final String table_course_backup = "course_backup";
    public static final String col_version = "version";
    public static final String event_insert = "INSERT";
    public static final String event_modify = "MODIFY";

    public static final String API_submitFeedback = "submitFeedback";
    public static final String table_feedback = "feedback";
    public static final String col_status = "status";
    public static final String status_NEW = "NEW";
    public static final String col_details = "details";
}
