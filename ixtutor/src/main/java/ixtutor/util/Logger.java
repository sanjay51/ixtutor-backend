package ixtutor.util;

import com.amazonaws.services.lambda.runtime.LambdaLogger;

/**
 * Created by sanjav on 1/8/17.
 */
public class Logger {
    private static LambdaLogger logger;

    public static void setLogger(LambdaLogger lambdaLogger) {
        logger = lambdaLogger;
    }

    public static LambdaLogger getLogger() {
        return logger;
    }

}
