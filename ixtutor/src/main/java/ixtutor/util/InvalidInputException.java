package ixtutor.util;

/**
 * Created by sanjav on 12/19/16.
 */
public class InvalidInputException extends RuntimeException {
    public InvalidInputException() {
        super();
    }

    public InvalidInputException(String message) {
        super(message);
    }
}
