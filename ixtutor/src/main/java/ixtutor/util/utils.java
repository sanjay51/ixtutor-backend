package ixtutor.util;

import com.amazonaws.util.StringUtils;
import ixtutor.security.TokenUtils;

/**
 * Created by sanjav on 12/25/16.
 */
public class utils {

    public static boolean isBlank(String input) {
        return StringUtils.isNullOrEmpty(StringUtils.trim(input));
    }

    public static void rejectBlankInput(String input, String message) {
        if (isBlank(input)) {
            throw new InvalidInputException(message);
        }
    }

    public static void verifyRequestAuthorization(String token, String id) {
        try {
            TokenUtils.verifyToken(token, id);
        } catch (Exception e) {
            throw new UnauthorizedException("Identity could not be verified.", e);
        }
    }
}