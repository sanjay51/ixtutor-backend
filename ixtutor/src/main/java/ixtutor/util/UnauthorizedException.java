package ixtutor.util;

/**
 * Created by sanjav on 12/19/16.
 */
public class UnauthorizedException extends RuntimeException {
    public UnauthorizedException() {
        super();
    }

    public UnauthorizedException(String message, Exception e) {
        super(message, e);
    }
}
