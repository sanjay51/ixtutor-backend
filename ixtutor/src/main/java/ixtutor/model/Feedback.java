package ixtutor.model;

import com.amazonaws.services.dynamodbv2.document.Item;

import static ixtutor.util.Constants.*;

/**
 * Created by sanjav on 12/25/16.
 */
public class Feedback {
    String id;
    String status;
    String category;
    String details;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Feedback() {}

    public Feedback(Item item) {
        this.id = item.getString(col_id);
        this.status = item.getString(col_status);
        this.category = item.getString(col_category);
        this.details = item.getString(col_details);
    }
}
