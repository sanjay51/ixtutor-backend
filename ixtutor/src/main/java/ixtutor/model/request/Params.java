package ixtutor.model.request;

import java.util.Arrays;
import java.util.Map;

/**
 * Created by sanjav on 12/18/16.
 */
public class Params {
    Path path;
    Map<String, String> querystring;
    Header header;

    public Params() {

    }

    public Map<String, String> getQuerystring() {
        return querystring;
    }

    public void setQuerystring(Map<String, String> querystring) {
        this.querystring = querystring;
    }

    @Override
    public String toString() {
        return "Params: " + Arrays.toString(this.querystring.entrySet().toArray());
    }
}