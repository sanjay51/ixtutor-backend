package ixtutor.model.request;

/**
 * Created by sanjav on 12/18/16.
 */
public class Context {
    String httpMethod;

    public Context() {
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    @Override
    public String toString() {
        return this.httpMethod;
    }
}