package ixtutor.model.request;

import java.util.LinkedHashMap;

public class Request {
    LinkedHashMap<String, String> body;
    Params params;
    Context context;
    
    public Context getContext() {
    	return this.context;
    }
    
    public void setContext(Context context) {
    	this.context = context;
    }

    public Params getParams() {
        return this.params;
    }

    public void setParams(Params params) {
        this.params = params;
    }

    public LinkedHashMap<String, String> getBody() {
        return body;
    }

    public void setBody(LinkedHashMap<String, String> body) {
        this.body = body;
    }

    public Request() {
    }
    
    @Override
    public String toString() {
    	return this.context.toString() + "---" + this.params.toString();
    }
}

