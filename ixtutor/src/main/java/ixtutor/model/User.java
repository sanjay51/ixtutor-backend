package ixtutor.model;

import com.amazonaws.services.dynamodbv2.document.Item;

import static ixtutor.util.Constants.*;

/**
 * Created by sanjav on 12/18/16.
 */
public class User {
    String email;
    String data;
    String id;
    String password;
    String token;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User() {

    }

    public User(Item item) {
        this.email = item.getString(col_email);
        this.password = item.getString(col_password);
        this.id = item.getString(col_id);
        this.data = item.getString(col_data);
    }
}
