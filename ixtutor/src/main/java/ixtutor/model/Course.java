package ixtutor.model;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import static ixtutor.util.Constants.*;

/**
 * Created by sanjav on 12/18/16.
 */
public class Course {
    String courseId;
    String metadata;
    String payload;
    static ObjectMapper objectMapper = new ObjectMapper();

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public Course() {}

    public Course(Item item) {
        this.courseId = item.getString(col_courseId);
        this.metadata = item.getString(col_metadata);
        this.payload = item.getString(col_payload);
    }

    public Course(String courseId, String metadata, String payload) {
        this.courseId = courseId;
        this.metadata = metadata;
        this.payload = payload;
    }

    public String getCourseAuthor() {
        Course.Metadata md;
        try {
            md = objectMapper.readValue(this.metadata, Course.Metadata.class);
        } catch (IOException e) {
            return null;
        }

        return md.getAuthor();
    }

    public static class Metadata {
        String author;
        String category;
        String title;
        String oneLineDescription;
        int beginChapterIndex;
        String description;

        public Metadata() {
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public void setBeginChapterIndex(int beginChapterIndex) {
            this.beginChapterIndex = beginChapterIndex;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setOneLineDescription(String oneLineDescription) {
            this.oneLineDescription = oneLineDescription;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
