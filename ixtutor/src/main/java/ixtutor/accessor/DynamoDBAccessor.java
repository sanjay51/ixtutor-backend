package ixtutor.accessor;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;

import static ixtutor.util.Constants.col_courseId;
import static ixtutor.util.Constants.col_metadata;

/**
 * Created by sanjav on 12/18/16.
 */
public class DynamoDBAccessor {
    private static DynamoDBAccessor INSTANCE;
    private static AmazonDynamoDBClient client;
    private static DynamoDB dynamoDB;

    private DynamoDBAccessor() {
        client = new AmazonDynamoDBClient()
                .withRegion(Regions.US_EAST_1);
        dynamoDB = new DynamoDB(client);
    }

    public static DynamoDBAccessor getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DynamoDBAccessor();
        }

        return INSTANCE;
    }

    public Item getItemByPrimaryKey(String tableName, String primaryKeyName, String value) {
        Table table = dynamoDB.getTable(tableName);

        GetItemSpec getItemSpec = new GetItemSpec()
                .withPrimaryKey(primaryKeyName, value);

        return table.getItem(getItemSpec);
    }

    public ItemCollection<ScanOutcome> scan(String tableName) {
        Table table = dynamoDB.getTable(tableName);
        ScanSpec scanSpec = new ScanSpec().withAttributesToGet(col_courseId, col_metadata);

        return table.scan(scanSpec);
    }

    public void putItem(String tableName, Item item) {
        Table table = dynamoDB.getTable(tableName);
        table.putItem(item);
    }

    public Item updateItem(String tableName, String primaryKeyName, String primaryKeyValue,
                           String updateExpression, ValueMap valueMap) {
        Table table = dynamoDB.getTable(tableName);

        UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                .withPrimaryKey(primaryKeyName, primaryKeyValue)
                .withUpdateExpression(updateExpression)
                .withValueMap(valueMap)
                .withReturnValues(ReturnValue.UPDATED_NEW);

        return table.updateItem(updateItemSpec).getItem();
    }
}
