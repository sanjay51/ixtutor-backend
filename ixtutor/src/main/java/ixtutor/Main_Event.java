package ixtutor;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import ixtutor.activities.createCourse.createCourseActivity;
import ixtutor.activities.updateCourseMetadata.updateCourseMetadataActivity;
import ixtutor.util.Logger;

import java.util.Map;

import static com.amazonaws.services.lambda.runtime.events.DynamodbEvent.DynamodbStreamRecord;
import static ixtutor.util.Constants.*;

public class Main_Event implements RequestHandler<DynamodbEvent, Response> {
    
    public Response handleRequest(DynamodbEvent event, Context context) {
        Logger.setLogger(context.getLogger());

        try {
            for (DynamodbStreamRecord record: event.getRecords()) {
                processRecord(record);
            }
        } catch (Exception e) {
            Logger.getLogger().log(e.toString());
            Logger.getLogger().log(context.getLogGroupName());
            Logger.getLogger().log(context.getLogStreamName());
        }

        Response response = new Response();
        response.setResponse(event.toString());
        return response;
    }

    private void processRecord(DynamodbStreamRecord record) {
        String eventName = record.getEventName();
        String tableName = getTableNameFromARN(record.getEventSourceARN());

        switch (eventName) {
            case event_insert:
                if (table_course.equalsIgnoreCase(tableName)) {
                    Map<String, AttributeValue> image = record.getDynamodb().getNewImage();
                    createCourseActivity.backup(image);
                }
                break;

            case event_modify:
                if (table_course.equalsIgnoreCase(tableName)) {
                    Map<String, AttributeValue> image = record.getDynamodb().getNewImage();
                    updateCourseMetadataActivity.backup(image);
                }
                break;

        }
    }

    private String getTableNameFromARN(String ddbARN) {
        return ddbARN.split(":")[5].split("/")[1];
    }
}