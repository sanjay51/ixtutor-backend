package ixtutor.activities.updateCourseMetadata;

import ixtutor.model.request.Request;

import java.util.Random;

import static ixtutor.util.Constants.*;

/**
 * Created by sanjav on 12/18/16.
 */
public class updateCourseMetadataRequest {
    Request request;
    static Random random = new Random();

    public updateCourseMetadataRequest(Request request) {
        this.request = request;
    }

    public String getCourseId() {
        return this.request.getParams().getQuerystring().get(col_courseId);
    }

    public String getMetadata() {
        return this.request.getParams().getQuerystring().get(col_metadata);
    }

    public String getIdentityToken() {
        return this.request.getParams().getQuerystring().get(param_token);
    }
}
