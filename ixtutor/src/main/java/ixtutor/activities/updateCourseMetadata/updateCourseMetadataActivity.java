package ixtutor.activities.updateCourseMetadata;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import ixtutor.Response;
import ixtutor.accessor.DynamoDBAccessor;
import ixtutor.activities.Activity;
import ixtutor.activities.getCourseByID.getCourseByIDActivity;
import ixtutor.model.Course;
import ixtutor.model.request.Request;
import ixtutor.util.utils;
import org.joda.time.DateTime;

import java.util.Map;

import static ixtutor.util.Constants.*;

public class updateCourseMetadataActivity extends Activity {
    updateCourseMetadataRequest request;
    static DynamoDBAccessor dynamoDBAccessor = DynamoDBAccessor.getInstance();

    public updateCourseMetadataActivity(Request request) {
        this.request = new updateCourseMetadataRequest(request);
    }

    @Override
    public Response enact() {
        String courseId = request.getCourseId();
        String metadata = request.getMetadata();

        String updateExpression = "set metadata = :metadata";
        ValueMap valueMap = new ValueMap()
                .withString(":metadata", metadata);

        Item updatedItem = dynamoDBAccessor.updateItem(table_course,
                col_courseId, courseId, updateExpression, valueMap);

        Response response = new Response();
        response.setResponse(new Course(updatedItem));

        return response;
    }

    public static void backup(Map<String, AttributeValue> attributeValueMap) {
        String courseId = attributeValueMap.get(col_courseId).getS();
        String metadata = attributeValueMap.get(col_metadata).getS();
        String payload = attributeValueMap.get(col_payload).getS();

        Item item = new Item()
                .withPrimaryKey(col_courseId, courseId)
                .withNumber(col_version, DateTime.now().getMillis())
                .withString(col_metadata, metadata)
                .withString(col_payload, payload);

        dynamoDBAccessor.putItem(table_course_backup, item);
    }

    @Override
    public void validateRequest() {
        utils.rejectBlankInput(request.getCourseId(), "Invalid courseId.");
        utils.rejectBlankInput(request.getMetadata(), "Invalid metadata.");

        Course course = getCourseByIDActivity.getCourseByID(request.getCourseId());
        utils.verifyRequestAuthorization(request.getIdentityToken(), course.getCourseAuthor());
    }
}
