package ixtutor.activities.getCourseByID;

import com.amazonaws.services.dynamodbv2.document.Item;
import ixtutor.Response;
import ixtutor.model.Course;
import ixtutor.accessor.DynamoDBAccessor;
import ixtutor.activities.Activity;
import ixtutor.model.request.Request;

import static ixtutor.util.Constants.col_courseId;
import static ixtutor.util.Constants.table_course;

/**
 * Created by sanjav on 12/18/16.
 */
public class getCourseByIDActivity extends Activity {
    getCourseByIDRequest request;
    static DynamoDBAccessor dynamoDBAccessor = DynamoDBAccessor.getInstance();

    public getCourseByIDActivity(Request request) {
        this.request = new getCourseByIDRequest(request);
    }

    @Override
    public Response enact() {
        Response response = new Response();
        response.setResponse(getCourseByID(request.getCourseId()));
        return response;
    }

    public static Course getCourseByID(String id) {
        Item item = dynamoDBAccessor.getItemByPrimaryKey(table_course, col_courseId, id);
        if (item == null) return null;

        return new Course(item);
    }

    @Override
    public void validateRequest() {

    }
}
