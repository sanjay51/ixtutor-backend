package ixtutor.activities.getCourseByID;

import ixtutor.model.request.Request;

import static ixtutor.util.Constants.col_courseId;

/**
 * Created by sanjav on 12/18/16.
 */
public class getCourseByIDRequest {
    Request request;

    public getCourseByIDRequest(Request request) {
        this.request = request;
    }

    public String getCourseId() {
        return this.request.getParams().getQuerystring().get(col_courseId);
    }
}
