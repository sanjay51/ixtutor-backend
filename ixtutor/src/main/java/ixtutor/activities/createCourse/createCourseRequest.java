package ixtutor.activities.createCourse;

import ixtutor.model.request.Request;

import java.util.Random;

import static ixtutor.util.Constants.*;

/**
 * Created by sanjav on 12/18/16.
 */
public class createCourseRequest {
    Request request;
    static Random random = new Random();

    public createCourseRequest(Request request) {
        this.request = request;
    }

    public String getCourseId() {
        String title = this.getTitle();

        String id = title.replaceAll("[^A-Za-z0-9-_]", "") + "-" + random.nextInt(1000);
        return id;
    }

    public String getTitle() {
        return this.request.getParams().getQuerystring().get(col_title);
    }

    public String getCategory() {
        return this.request.getParams().getQuerystring().get(col_category);
    }

    public String getOneLineDescription() {
        return this.request.getParams().getQuerystring().get(col_oneLineDescription);
    }

    public String getAuthor() {
        return this.request.getParams().getQuerystring().get(col_author);
    }

    public String getDescription() {
        return this.request.getParams().getQuerystring().get(col_description);
    }

    public String getIdentityToken() {
        return this.request.getParams().getQuerystring().get(param_token);
    }
}
