package ixtutor.activities.createCourse;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import ixtutor.Response;
import ixtutor.accessor.DynamoDBAccessor;
import ixtutor.activities.Activity;
import ixtutor.model.Course;
import ixtutor.model.request.Request;
import ixtutor.util.utils;

import java.util.Map;

import static ixtutor.util.Constants.*;

public class createCourseActivity extends Activity {
    createCourseRequest request;
    static DynamoDBAccessor dynamoDBAccessor = DynamoDBAccessor.getInstance();

    public createCourseActivity(Request request) {
        this.request = new createCourseRequest(request);
    }

    @Override
    public Response enact() {
        String metadata = getFormattedMetadata(request.getCategory(),
                request.getTitle(), request.getOneLineDescription(),
                request.getDescription(), request.getAuthor());
        String payload = getSamplePayload();
        Item item = new Item()
                .withPrimaryKey(col_courseId, request.getCourseId())
                .withString(col_metadata, metadata)
                .withString(col_payload, payload);

        dynamoDBAccessor.putItem(table_course, item);

        Response response = new Response();
        response.setResponse(new Course(item));

        return response;
    }

    public static void backup(Map<String, AttributeValue> attributeValueMap) {
        String courseId = attributeValueMap.get(col_courseId).getS();
        String metadata = attributeValueMap.get(col_metadata).getS();
        String payload = attributeValueMap.get(col_payload).getS();

        Item item = new Item()
                .withPrimaryKey(col_courseId, courseId)
                .withNumber(col_version, 0)
                .withString(col_metadata, metadata)
                .withString(col_payload, payload);

        dynamoDBAccessor.putItem(table_course_backup, item);
    }

    @Override
    public void validateRequest() {
        utils.rejectBlankInput(request.getTitle(), "Invalid title.");
        utils.rejectBlankInput(request.getCategory(), "Invalid category.");
        utils.rejectBlankInput(request.getDescription(), "Invalid description.");
        utils.rejectBlankInput(request.getAuthor(), "Invalid author.");
        utils.rejectBlankInput(request.getIdentityToken(), "Invalid identity token.");

        utils.verifyRequestAuthorization(request.getIdentityToken(), request.getAuthor());
    }

    private String getFormattedMetadata(String category,
                                        String title,
                                        String oneLineDescription,
                                        String description,
                                        String author) {
        String format = "{\"category\": \"%s\", \"title\": \"%s\", " +
                "\"oneLineDescription\": \"%s\", \"beginChapterIndex\": 0," +
                "\"description\": \"%s\", \"author\": \"%s\" }";
        return String.format(format, category, title, oneLineDescription, description, author);
    }

    private String getSamplePayload() {
        return "[{\"sections\":[{\"validInputs\":[\"Hello World\"],\"id\":0," +
                "\"title\":\"Sample section 1\",\"instruction\":" +
                "{\"text\":\"Section 1 - Type 'hello world' to continue.\"}," +
                "\"inputPlaceholder\":\"Type 'hello world' to continue.\"," +
                "\"output\":{\"text\":\"Click Next to continue\"}," +
                "\"policy\":{\"statements\":[{\"conditions\":[" +
                "{\"conditionId\":0,\"lhs\":\"$inputIdeal.toString().toLowerCase()\"," +
                "\"operator\":\"==\",\"rhs\":\"$inputReal.toString().toLowerCase()\"}]," +
                "\"statementId\":0,\"description\":\"Sample statement - 1\"}]}}],\"id\":0," +
                "\"meta\":{\"title\":\"Sample Chapter - 1\",\"beginSectionIndex\":0}}]";
    }
}
