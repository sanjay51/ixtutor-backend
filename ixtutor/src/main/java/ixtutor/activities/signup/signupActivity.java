package ixtutor.activities.signup;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.util.StringUtils;
import ixtutor.Response;
import ixtutor.accessor.DynamoDBAccessor;
import ixtutor.activities.Activity;
import ixtutor.model.User;
import ixtutor.model.request.Request;
import ixtutor.util.InvalidInputException;
import org.joda.time.DateTime;

import java.util.UUID;

import static ixtutor.util.Constants.*;

public class signupActivity extends Activity {
    signupRequest request;
    DynamoDBAccessor dynamoDBAccessor = DynamoDBAccessor.getInstance();

    public signupActivity(Request request) {
        this.request = new signupRequest(request);
    }

    @Override
    public Response enact() {
        String id = UUID.randomUUID().toString();
        Item item = new Item()
                .withPrimaryKey(col_email, request.getUserEmail())
                .withString(col_id, id)
                .withString(col_password, request.getUserPassword())
                .withString(col_data, getFormattedData(request.getUserName()));

        dynamoDBAccessor.putItem(table_user, item);

        Response response = new Response();
        response.setResponse(new User(item));

        return response;
    }

    @Override
    public void validateRequest() {
        if (isBlank(request.getUserName())) {
            throw new InvalidInputException("Invalid name.");
        }

        if (isBlank(request.getUserPassword())) {
            throw new InvalidInputException("Invalid password.");
        }

        if (isBlank(request.getUserEmail())) {
            throw new InvalidInputException("Invalid email.");
        }
    }

    private boolean isBlank(String input) {
        return StringUtils.isNullOrEmpty(StringUtils.trim(input));
    }

    private String getFormattedData(String name) {
        String format = "{\"name\": \"%s\", \"status\": \"ACTIVE\", " +
                "\"creation_timestamp\": \"%s\"," +
                "\"courses_authored\": [], \"courses_enrolled\": [] }";
        return String.format(format, name, DateTime.now());
    }
}
