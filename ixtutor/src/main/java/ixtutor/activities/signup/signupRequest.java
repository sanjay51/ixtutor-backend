package ixtutor.activities.signup;

import ixtutor.model.request.Request;

import static ixtutor.util.Constants.col_email;
import static ixtutor.util.Constants.col_name;
import static ixtutor.util.Constants.col_password;

/**
 * Created by sanjav on 12/18/16.
 */
public class signupRequest {
    Request request;

    public signupRequest(Request request) {
        this.request = request;
    }

    public String getUserEmail() {
        return this.request.getParams().getQuerystring().get(col_email);
    }

    public String getUserPassword() {
        return this.request.getParams().getQuerystring().get(col_password);
    }

    public String getUserName() {
        return this.request.getParams().getQuerystring().get(col_name);
    }
}
