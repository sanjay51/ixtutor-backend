package ixtutor.activities.updateCoursePayload;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.util.StringUtils;
import ixtutor.Response;
import ixtutor.accessor.DynamoDBAccessor;
import ixtutor.activities.Activity;
import ixtutor.activities.getCourseByID.getCourseByIDActivity;
import ixtutor.model.Course;
import ixtutor.model.request.Request;
import ixtutor.util.utils;

import static ixtutor.util.Constants.col_courseId;
import static ixtutor.util.Constants.table_course;

public class updateCoursePayloadActivity extends Activity {
    updateCoursePayloadRequest request;
    DynamoDBAccessor dynamoDBAccessor = DynamoDBAccessor.getInstance();

    public updateCoursePayloadActivity(Request request) {
        this.request = new updateCoursePayloadRequest(request);
    }

    @Override
    public Response enact() {
        String courseId = request.getCourseId();
        String payload = request.getPayload();

        String updateExpression = "set payload = :payload";
        ValueMap valueMap = new ValueMap()
                .withString(":payload", payload);

        Item updatedItem = dynamoDBAccessor.updateItem(table_course,
                col_courseId, courseId, updateExpression, valueMap);

        Response response = new Response();
        response.setResponse(new Course(updatedItem));

        return response;
    }

    @Override
    public void validateRequest() {
        utils.rejectBlankInput(request.getCourseId(), "Invalid courseId.");
        utils.rejectBlankInput(request.getPayload(), "Invalid payload.");

        Course course = getCourseByIDActivity.getCourseByID(request.getCourseId());
        utils.verifyRequestAuthorization(request.getIdentityToken(), course.getCourseAuthor());
    }

    private boolean isBlank(String input) {
        return StringUtils.isNullOrEmpty(StringUtils.trim(input));
    }
}
