package ixtutor.activities.updateCoursePayload;

import ixtutor.model.request.Request;

import java.util.Random;

import static ixtutor.util.Constants.col_courseId;
import static ixtutor.util.Constants.col_payload;
import static ixtutor.util.Constants.param_token;

/**
 * Created by sanjav on 12/18/16.
 */
public class updateCoursePayloadRequest {
    Request request;
    static Random random = new Random();

    public updateCoursePayloadRequest(Request request) {
        this.request = request;
    }

    public String getCourseId() {
        return this.request.getParams().getQuerystring().get(col_courseId);
    }

    public String getPayload() {
        return this.request.getParams().getQuerystring().get(col_payload);
    }

    public String getIdentityToken() {
        return this.request.getParams().getQuerystring().get(param_token);
    }
}
