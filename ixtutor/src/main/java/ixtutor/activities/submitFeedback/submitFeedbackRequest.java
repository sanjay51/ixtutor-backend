package ixtutor.activities.submitFeedback;

import ixtutor.model.request.Request;

import static ixtutor.util.Constants.col_category;
import static ixtutor.util.Constants.col_details;

public class submitFeedbackRequest {
    Request request;

    public submitFeedbackRequest(Request request) {
        this.request = request;
    }

    public String getCategory() {
        return this.request.getParams().getQuerystring().get(col_category);
    }

    public String getDetails() {
        return this.request.getBody().get(col_details);
    }
}
