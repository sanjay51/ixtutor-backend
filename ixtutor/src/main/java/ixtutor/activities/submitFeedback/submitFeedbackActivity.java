package ixtutor.activities.submitFeedback;

import com.amazonaws.services.dynamodbv2.document.Item;
import ixtutor.Response;
import ixtutor.accessor.DynamoDBAccessor;
import ixtutor.activities.Activity;
import ixtutor.model.Feedback;
import ixtutor.model.request.Request;
import ixtutor.util.InvalidInputException;

import java.util.UUID;

import static ixtutor.util.Constants.*;
import static ixtutor.util.utils.isBlank;

public class submitFeedbackActivity extends Activity {
    submitFeedbackRequest request;
    DynamoDBAccessor dynamoDBAccessor = DynamoDBAccessor.getInstance();

    public submitFeedbackActivity(Request request) {
        this.request = new submitFeedbackRequest(request);
    }

    @Override
    public Response enact() {
        String id = UUID.randomUUID().toString();

        Item item = new Item()
                .withPrimaryKey(col_id, id)
                .withString(col_status, status_NEW)
                .withString(col_category, request.getCategory())
                .withString(col_details, request.getDetails());

        dynamoDBAccessor.putItem(table_feedback, item);

        Response response = new Response();
        response.setResponse(new Feedback(item));

        return response;
    }

    @Override
    public void validateRequest() {
        if (isBlank(request.getCategory())) {
            throw new InvalidInputException("Invalid category.");
        }

        if (isBlank(request.getDetails())) {
            throw new InvalidInputException("Invalid details.");
        }
    }
}
