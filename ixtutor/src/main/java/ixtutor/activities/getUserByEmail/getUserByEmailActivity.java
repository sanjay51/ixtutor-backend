package ixtutor.activities.getUserByEmail;

import com.amazonaws.services.dynamodbv2.document.Item;
import ixtutor.Response;
import ixtutor.accessor.DynamoDBAccessor;
import ixtutor.activities.Activity;
import ixtutor.model.User;
import ixtutor.model.request.Request;

import static ixtutor.util.Constants.col_email;
import static ixtutor.util.Constants.table_user;

public class getUserByEmailActivity extends Activity {
    getUserByEmailRequest request;
    DynamoDBAccessor dynamoDBAccessor = DynamoDBAccessor.getInstance();

    public getUserByEmailActivity(Request request) {
        this.request = new getUserByEmailRequest(request);
    }

    @Override
    public Response enact() {
        Item item = dynamoDBAccessor.getItemByPrimaryKey(table_user, col_email, request.getUserEmail());

        if (item == null) return null;

        Response response = new Response();
        response.setResponse(new User(item));
        return response;
    }

    @Override
    public void validateRequest() {

    }
}
