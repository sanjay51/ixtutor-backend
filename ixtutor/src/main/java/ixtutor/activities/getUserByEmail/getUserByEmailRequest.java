package ixtutor.activities.getUserByEmail;

import ixtutor.model.request.Request;

import static ixtutor.util.Constants.col_email;

/**
 * Created by sanjav on 12/18/16.
 */
public class getUserByEmailRequest {
    Request request;

    public getUserByEmailRequest(Request request) {
        this.request = request;
    }

    public String getUserEmail() {
        return this.request.getParams().getQuerystring().get(col_email);
    }
}
