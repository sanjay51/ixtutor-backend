package ixtutor.activities;

import ixtutor.activities.createCourse.createCourseActivity;
import ixtutor.activities.getAllCoursesMetadata.getAllCoursesMetadataActivity;
import ixtutor.activities.getCourseByID.getCourseByIDActivity;
import ixtutor.activities.getUserByEmail.getUserByEmailActivity;
import ixtutor.activities.login.loginActivity;
import ixtutor.activities.signup.signupActivity;
import ixtutor.activities.submitFeedback.submitFeedbackActivity;
import ixtutor.activities.updateCourseMetadata.updateCourseMetadataActivity;
import ixtutor.activities.updateCoursePayload.updateCoursePayloadActivity;
import ixtutor.model.request.Request;

import static ixtutor.util.Constants.*;

/**
 * Created by sanjav on 12/18/16.
 */

public class ActivityFactory {
    public static Activity getInstance(Request request) {
        String API = request.getParams().getQuerystring().get(PARAM_API);

        switch (API) {
            case API_getCourseByID:
                return new getCourseByIDActivity(request);

            case API_getAllCoursesMetadata:
                return new getAllCoursesMetadataActivity(request);

            case API_getUserByEmail:
                return new getUserByEmailActivity(request);

            case API_login:
                return new loginActivity(request);

            case API_signup:
                return new signupActivity(request);

            case API_createCourse:
                return new createCourseActivity(request);

            case API_updateCourseMetadata:
                return new updateCourseMetadataActivity(request);

            case API_updateCoursePayload:
                return new updateCoursePayloadActivity(request);

            case API_submitFeedback:
                return new submitFeedbackActivity(request);

            default:
                throw new UnsupportedOperationException("API not supported.");
        }
    }
}
