package ixtutor.activities.login;

import com.amazonaws.services.dynamodbv2.document.Item;
import ixtutor.Response;
import ixtutor.accessor.DynamoDBAccessor;
import ixtutor.activities.Activity;
import ixtutor.model.User;
import ixtutor.model.request.Request;
import ixtutor.security.TokenUtils;

import static ixtutor.util.Constants.col_email;
import static ixtutor.util.Constants.table_user;

public class loginActivity extends Activity {
    loginRequest request;
    DynamoDBAccessor dynamoDBAccessor = DynamoDBAccessor.getInstance();

    public loginActivity(Request request) {
        this.request = new loginRequest(request);
    }

    @Override
    public Response enact() {
        Item item = dynamoDBAccessor.getItemByPrimaryKey(table_user, col_email, request.getUserEmail());
        if (item == null) return null;

        User user = new User(item);
        user.setToken(TokenUtils.generateToken(user.getId()));

        Response response = new Response();
        if (user.getPassword() != null && user.getPassword().equals(request.getUserPassword())) {
            response.setResponse(user);
        } else {
            response.setResponse("false");
        }

        return response;
    }

    @Override
    public void validateRequest() {

    }
}
