package ixtutor.activities.login;

import ixtutor.model.request.Request;

import static ixtutor.util.Constants.col_email;
import static ixtutor.util.Constants.col_password;

/**
 * Created by sanjav on 12/18/16.
 */
public class loginRequest {
    Request request;

    public loginRequest(Request request) {
        this.request = request;
    }

    public String getUserEmail() {
        return this.request.getParams().getQuerystring().get(col_email);
    }

    public String getUserPassword() {
        return this.request.getParams().getQuerystring().get(col_password);
    }
}
