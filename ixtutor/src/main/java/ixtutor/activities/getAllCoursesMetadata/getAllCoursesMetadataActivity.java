package ixtutor.activities.getAllCoursesMetadata;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import ixtutor.Response;
import ixtutor.model.Course;
import ixtutor.accessor.DynamoDBAccessor;
import ixtutor.activities.Activity;
import ixtutor.model.request.Request;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static ixtutor.util.Constants.table_course;

/**
 * Created by sanjav on 12/18/16.
 */
public class getAllCoursesMetadataActivity extends Activity {
    getAllCoursesMetadataRequest request;
    DynamoDBAccessor dynamoDBAccessor = DynamoDBAccessor.getInstance();
    private static List<Course> courses;

    public getAllCoursesMetadataActivity(Request request) {
        this.request = new getAllCoursesMetadataRequest(request);
    }

    @Override
    public Response enact() {

        if (courses == null) {
            ItemCollection items = dynamoDBAccessor.scan(table_course);
            if (items == null) return null;

            courses = new ArrayList<>();
            Iterator<Item> iterator = items.iterator();
            while (iterator.hasNext()) {
                Item item = iterator.next();

                courses.add(new Course(item));
            }
        }

        Response response = new Response();
        response.setResponse(courses);
        return response;
    }

    @Override
    public void validateRequest() {

    }
}
