package ixtutor.activities;

import ixtutor.Response;
import ixtutor.util.InvalidInputException;

/**
 * Created by sanjav on 12/18/16.
 */
public abstract class Activity {
    public abstract Response enact();
    public abstract void validateRequest() throws InvalidInputException;

    public Response getResponse() throws RuntimeException {
        this.validateRequest();
        return this.enact();
    }
}
